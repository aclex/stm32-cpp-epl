/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include <array>

#include <epl/gpio.h>
#include <epl/timer.h>
#include <epl/dma.h>

using namespace std;

using namespace stm32::epl;

namespace
{
	constexpr array<uint16_t, 40> buffer =
	{
		1, 2, 3, 4,
		2, 3, 4, 5,
		3, 4, 5, 6,
		4, 5, 6, 7,
		5, 6, 7, 8,
		6, 7, 8, 9,
		7, 8, 9, 10,
		8, 9, 10, 1,
		9, 10, 1, 2,
		10, 1, 2, 3
	};

	constexpr size_t s_period(11);
	constexpr size_t s_frequency(6 * 1000000); // 6 MHz
	constexpr size_t s_channel_count(4);
}

int main(void)
{
	timer::tim<1>::configure
	<
		/* period */ s_period,
		/* prescaler */ mcu::rcc::system_clock_frequency / s_frequency - 1
	>();

	timer::tim<1>::channel<1, direction::out>::configure
	<
		timer::output_compare_mode::pwm1,
		timer::polarity::active_high,
		gpio::pin<mcu::gpio::PA, 8>
	>();

	timer::tim<1>::channel<1, direction::out>::set_preload(state::on);
	timer::tim<1>::channel<1, direction::out>::enable();

	timer::tim<1>::channel<2, direction::out>::configure
	<
		timer::output_compare_mode::pwm1,
		timer::polarity::active_high,
		gpio::pin<mcu::gpio::PA, 9>
		>();

	timer::tim<1>::channel<2, direction::out>::set_preload(state::on);
	timer::tim<1>::channel<2, direction::out>::enable();

	timer::tim<1>::channel<3, direction::out>::configure
	<
		timer::output_compare_mode::pwm1,
		timer::polarity::active_high,
		gpio::pin<mcu::gpio::PA, 10>
		>();

	timer::tim<1>::channel<3, direction::out>::set_preload(state::on);
	timer::tim<1>::channel<3, direction::out>::enable();

	timer::tim<1>::channel<4, direction::out>::configure
	<
		timer::output_compare_mode::pwm1,
		timer::polarity::active_high,
		gpio::pin<mcu::gpio::PA, 11>
		>();

	timer::tim<1>::channel<4, direction::out>::set_preload(state::on);
	timer::tim<1>::channel<4, direction::out>::enable();

	timer::tim<1>::set_dma_burst_length(s_channel_count);
	timer::tim<1>::set_dma_burst_target(timer::tim<1>::channel<1, direction::out>::burst_target::ccr());
	timer::tim<1>::set_dma_request(timer::dma::request::update, state::on);

	timer::tim<1>::start();

	// TIM1 update DMA request is routed to DMA channel 5
	dma::stream<5>::configure<dma::source::memory, dma::source::peripheral, dma::mode::circular>();
	dma::stream<5>::set_address<dma::source::memory>(buffer.data());
	dma::stream<5>::set_address<dma::source::peripheral>(timer::tim<1>::reg::dmar());
	dma::stream<5>::set_number_of_data(buffer.size());

	dma::stream<5>::set_increment<dma::source::memory>(state::on);
	dma::stream<5>::set_increment<dma::source::peripheral>(state::off);

	dma::stream<5>::enable();

	while(true);
}

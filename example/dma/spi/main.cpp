/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include <array>
#include <span>

#include <epl/thread.h>
#include <epl/spi.h>
#include <epl/dma.h>

using namespace std;

using namespace epl;

namespace
{
	using miso = gpio::pin<gpio::port::a, 6>;
	using mosi = gpio::pin<gpio::port::a, 7>;
	using sck = gpio::pin<gpio::port::a, 5>;
	using nss = gpio::pin<gpio::port::a, 4>;

	using spi0 = spi::port<0, sck, miso, mosi, nss>;

	using buffer_type = array<uint8_t, 4>;
	using buffer_span_type = span<uint8_t, 4>;

	using rx_dma_stream_type = dma::stream<spi::dma::slot<spi0>, dma::memory::slot<buffer_span_type>>;
	using tx_dma_stream_type = dma::stream<dma::memory::slot<buffer_span_type>, spi::dma::slot<spi0>>;

	template<class D> class irq_handler : public D
	{
	public:
		void on_event(const dma::irq::event::full_transfer_finish& e) noexcept override
		{
			D::on_event(e);
			finish();
		}

	private:
		void finish() noexcept
		{
			spi0::disable();
		}
	};

	using my_irq_handler_type = irq_handler<rx_dma_stream_type::basic_irq_handler_type>;
}

int main(int, char**)
{
	epl::irq::unit::reset();
	epl::irq::unit::set_level_bits(3);

	epl::irq::unit::enable();

	spi0::configure(spi::mode::master, duplex::full, spi::word_length::b8, spi::prescaler::p8, spi::bit_order::msb, spi::nss_mode::output);

	buffer_type rx_buf{};
	buffer_type tx_buf{0xde, 0xab, 0x11, 0x05};

	rx_dma_stream_type::configure(rx_buf.data());
	tx_dma_stream_type::configure(tx_buf.data());

	rx_dma_stream_type::emplace_irq_handling<my_irq_handler_type>();

	while(true)
	{
		rx_dma_stream_type::enable();
		tx_dma_stream_type::enable(/* disable_on_full_transfer = */true);
		spi0::enable();

		this_thread::sleep_for(1s);
	}

	rx_dma_stream_type::disable_irq_handling();
}

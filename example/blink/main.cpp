/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include <string>

#include <epl/thread.h>
#include <epl/gpio.h>
#include <epl/irq.h>

using namespace std;

using namespace epl;

int main(void)
{
	using led = gpio::pin<gpio::port::b, 0>;
	led::configure(gpio::output_mode::push_pull); // or gpio::configure<led>(...)

	while(true)
	{
		led::toggle(); // or gpio::toggle<led>();
		this_thread::sleep_for(1s);
	}
}

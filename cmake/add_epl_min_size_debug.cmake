macro(add_epl_min_size_debug_build_type)
	set(CMAKE_CXX_FLAGS_EPLMINSIZEDEBUG "-Os -g" CACHE STRING
		"Flags used by the C++ compiler during EplMinSizeDebug builds."
		FORCE)
	set(CMAKE_C_FLAGS_EPLMINSIZEDEBUG "-Os -g" CACHE STRING
		"Flags used by the C compiler during EplMinSizeDebug builds."
		FORCE)
	set(CMAKE_EXE_LINKER_FLAGS_EPLMINSIZEDEBUG
		"" CACHE STRING
		"Flags used for linking binaries during EplMinSizeDebug builds."
		FORCE)
	set(CMAKE_SHARED_LINKER_FLAGS_EPLMINSIZEDEBUG
		"" CACHE STRING
		"Flags used by the shared libraries linker during EplMinSizeDebug builds."
		FORCE)
	mark_as_advanced(
		CMAKE_CXX_FLAGS_EPLMINSIZEDEBUG
		CMAKE_C_FLAGS_EPLMINSIZEDEBUG
		CMAKE_EXE_LINKER_FLAGS_EPLMINSIZEDEBUG
		CMAKE_SHARED_LINKER_FLAGS_EPLMINSIZEDEBUG)
# Update the documentation string of CMAKE_BUILD_TYPE for GUIs
	set(CMAKE_BUILD_TYPE "${CMAKE_BUILD_TYPE}" CACHE STRING
		"Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel EplMinSizeDebug."
		FORCE)
endmacro()

if (NOT EPL_CLIENT_PROJECT_INITIALIZED)
	set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_LIST_DIR})

	include(mcu_info)

	set(MCU_MODEL "gd32vf103cb" CACHE STRING "Target MCU model (as exact, as possible, e.g.: gd32vf103cb)")
	set(CMAKE_TRY_COMPILE_PLATFORM_VARIABLES MCU_MODEL)

	get_mcu_info(${MCU_MODEL})

	message("MCU generation: ${MCU_GEN}")
	message("MCU family: ${MCU_FAMILY}")
	message("MCU line: ${MCU_LINE}")

	set(CMAKE_TOOLCHAIN_FILE ${CMAKE_CURRENT_LIST_DIR}/toolchain/${MCU_GEN}/${MCU_FAMILY}/toolchain.cmake)

	set(EPL_CLIENT_PROJECT_INITIALIZED TRUE)
endif()

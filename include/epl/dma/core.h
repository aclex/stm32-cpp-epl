/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_DMA_H
#define STM32_CPP_EPL_DMA_H

#include <epl/mcu.h>

#include <epl/dma/mode.h>
#include <epl/dma/source.h>
#include <epl/dma/priority.h>
#include <epl/dma/interrupt.h>

namespace stm32
{
	namespace epl
	{
		namespace dma
		{
			template<std::size_t no> class stream
			{
			private:
				typedef mcu::dma<no> hw;

			public:
				template<source src, source dest, mode m>
				static constexpr void configure() noexcept
				{
					stream::disable();

					hw::clock_bus::template enable_clock<hw::clock_flag>();

					stream::set_direction(src, dest);
					stream::set_mode(m);
				}

				static constexpr mode current_mode() noexcept
				{
					return hw::current_mode();
				}

				static constexpr void set_mode(mode m) noexcept
				{
					hw::set_mode(m);
				}

				static constexpr void set_direction(source src, source dest) noexcept
				{
					hw::set_direction(src, dest);
				}

				template<source src, typename T>
				static constexpr void set_address(T* addr) noexcept
				{
					hw::template set_address<src>(addr);
				}

				template<source src, std::size_t transfer_size>
				static constexpr void set_transfer_size() noexcept
				{
					hw::template set_transfer_size<src, transfer_size>();
				}

				static constexpr std::size_t number_of_data() noexcept
				{
					return hw::number_of_data();
				}

				static constexpr void set_number_of_data(std::size_t number) noexcept
				{
					hw::set_number_of_data(number);
				}

				template<source src>
				static constexpr void set_increment(epl::state st) noexcept
				{
					hw::template set_increment<src>(st);
				}

				static constexpr priority current_priority() noexcept
				{
					return hw::current_priority();
				}

				static constexpr void set_priority(priority pr) noexcept
				{
					hw::set_priority(pr);
				}

				static constexpr bool enabled() noexcept
				{
					return hw::enabled();
				}

				static constexpr void enable() noexcept
				{
					hw::enable();
				}

				static constexpr void disable() noexcept
				{
					hw::disable();
				}

				template<interrupt ir, std::size_t priority = 1> static void set_interrupt_handler(void (* const f)()) noexcept
				{
					hw::template set_interrupt_handler<ir, priority>(f);
				}
			};
		}
	}
}

#endif // STM32_CPP_EPL_DMA_H

/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef EPL_SPI_CLOCK_H
#define EPL_SPI_CLOCK_H

namespace epl::spi
{
	enum class clock_polarity : unsigned char // idle clock state
	{
		low = 0,
		high = 1
	};

	enum class clock_phase : unsigned char // latch edge
	{
		front = 0,
		rear = 1
	};
}

#endif // EPL_SPI_CLOCK_H

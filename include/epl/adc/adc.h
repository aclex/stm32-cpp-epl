/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_ADC_ADC_H
#define STM32_CPP_EPL_ADC_ADC_H

#include <epl/mcu.h>

#include <epl/adc/ongoing_operation_handling.h>
#include <epl/adc/awd_channel_selection.h>
#include <epl/adc/conversion_mode.h>
#include <epl/adc/overrun_mode.h>
#include <epl/adc/external_trigger_mode.h>
#include <epl/adc/data_alignment.h>
#include <epl/adc/scan_direction.h>
#include <epl/adc/clock_mode.h>
#include <epl/adc/dma_mode.h>

namespace stm32
{
	namespace epl
	{
		namespace adc
		{
			template<std::size_t no> class adc
			{
				typedef mcu::adc<no> device;

			public:
				struct reg
				{
					static constexpr volatile std::uint32_t* dr() noexcept
					{
						return device::reg::dr();
					}
				};

				static void configure() noexcept
				{
					device::clock_bus::template enable_clock<device::clock_flag>();
				}

				template<typename... Channels> static void set_active_channels() noexcept
				{
					device::template set_active_channels<enumerate_channels<Channels...>()>();
					pass((configure_channel<Channels>(), 0)...);
				}

				static void calibrate() noexcept
				{
					disable<ongoing_operation_handling::stop>();
					device::calibrate();

					while (calibrating());
				}

				static bool calibrating() noexcept
				{
					return device::calibrating();
				}

				static void enable() noexcept
				{
					device::clear_ready();
					device::enable();

					while (!device::ready());
				}

				static bool enabled() noexcept
				{
					return device::enabled();
				}

				template<ongoing_operation_handling conversion_action = ongoing_operation_handling::stop>
				static void disable() noexcept
				{
					if (!enabled())
						return;

					if (ongoing_conversion())
					{
						if constexpr (conversion_action == ongoing_operation_handling::stop)
						{
							stop_conversion();
						}
						else
						{
							while (ongoing_conversion());
						}
					}

					device::disable();

					while (enabled());

					device::clear_ready();
				}

				static void start_conversion() noexcept
				{
					if (!enabled())
						return;

					device::start_conversion();
				}

				static bool ongoing_conversion() noexcept
				{
					return device::ongoing_conversion();
				}

				static void stop_conversion() noexcept
				{
					if (!enabled() || device::stopping_conversion() || !ongoing_conversion())
						return;

					device::stop_conversion();

					while (ongoing_conversion() || device::stopping_conversion());
				}

				static std::uint16_t value() noexcept
				{
					return device::value();
				}

				static bool ready() noexcept
				{
					return device::ready();
				}

				template<typename Channel> void select_for_analog_watchdog() noexcept
				{
					if (!enabled())
						return;

					device::template select_for_analog_watchdog<Channel>();
				}

				template<state st> static void switch_analog_watchdog() noexcept
				{
					if (!enabled())
						return;

					device::template switch_analog_watchdog<st>();
				}

				template<awd_channel_selection aws>
				static void set_analog_watchdog_channel_selection() noexcept
				{
					if (!enabled())
						return;

					device::template set_analog_watchdog_channels<aws>();
				}

				template<conversion_mode m> static void set_mode() noexcept
				{
					if (!enabled())
						return;

					device::template set_mode<m>();
				}

				template<state st> static void switch_auto_off_mode() noexcept
				{
					if (!enabled())
						return;

					device::template switch_auto_off<st>();
				}

				template<state st> static void switch_wait_mode() noexcept
				{
					if (!enabled())
						return;

					device::template switch_wait<st>();
				}

				template<overrun_mode ovm> static void set_overrun_management_mode() noexcept
				{
					if (!enabled())
						return;

					device::template set_overrun_management_mode<ovm>();
				}

				template<external_trigger_mode ext = external_trigger_mode::disabled, std::size_t trigger_source = 0> void configure_external_trigger() noexcept
				{
					if (!enabled())
						return;

					device::template configure_external_trigger<trigger_source>();
				}

				template<data_alignment al> static void set_data_alignment() noexcept
				{
					if (!enabled())
						return;

					device::template set_data_alignment<al>();
				}

				static std::size_t data_resolution() noexcept
				{
					return device::data_resolution();
				}

				template<std::size_t res> static void set_data_resolution() noexcept
				{
					if (enabled())
						return;

					device::template set_data_resolution<res>();
				}

				template<scan_direction dir> static void set_scan_direction() noexcept
				{
					if (!enabled())
						return;

					device::template set_scan_direction<dir>();
				}

				template<clock_mode clkm> static void set_clock_mode() noexcept
				{
					if (!enabled())
						return;

					device::template set_clock_mode<clkm>();
				}

				static void set_sampling_time(const float sampling_time) noexcept
				{
					if (!enabled())
						return;

					device::set_sampling_time(sampling_time);
				}

				template<unsigned int low, unsigned int high> static void set_analog_watchdog_thresholds() noexcept
				{
					if (!enabled())
						return;

					device::template set_analog_watchdog_thresholds<low, high>();
				}

				template<state st, dma_mode dm = dma_mode::one_shot> static void set_dma_request() noexcept
				{
					device::template set_dma_request<st, dm>();
				}

				static std::uint16_t vrefint_cal() noexcept
				{
					return device::vrefint_cal();
				}

			private:
				template<typename Channel> static void configure_channel() noexcept
				{
					if constexpr (!std::is_same<Channel, temp_sensor>::value &&
						!std::is_same<Channel, vref>::value)
					{
						Channel::template configure<gpio::mode::analog>();
					}
					else if constexpr (std::is_same<Channel, temp_sensor>::value)
					{
						device::template switch_temp_sensing<state::on>();
					}
					else if constexpr (std::is_same<Channel, vref>::value)
					{
						device::template switch_vref_sensing<state::on>();
					}
				}

				template<typename Channel> constexpr static std::uint32_t enumerate_channel() noexcept
				{
					if constexpr (!std::is_same<Channel, temp_sensor>::value &&
						!std::is_same<Channel, vref>::value)
					{
						return device::channel_mapper::template get_channel(Channel::port_name, Channel::pin_no);
					}
					else
					{
						return device::channel_mapper::template get_channel<Channel>();
					}
				}

				template<typename... Channels>
				static constexpr std::uint32_t enumerate_channels() noexcept
				{
					return (... + (1 << enumerate_channel<Channels>()));
				}
			};
		}
	}
}

#endif // STM32_CPP_EPL_ADC_ADC_H

/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef EPL_UTIL_H
#define EPL_UTIL_H

#include <array>
#include <type_traits>
#include <cstdint>

namespace epl
{
	template<typename T> class safe_int_to_ptr
	{
	public:
		constexpr safe_int_to_ptr(std::intptr_t ip) : m_addr(ip) { }
		operator T*() const { return reinterpret_cast<T*>(m_addr); }
		T* operator->() const { return operator T*(); }

	private:
		std::intptr_t m_addr;
	};

	inline std::uint32_t compress(const std::uint32_t x, const std::uint32_t mask) noexcept
	{
		std::uint32_t mk {~mask << 1};
		std::uint32_t op {x & mask}, m {mask};

		constexpr size_t iter_count { 5 }; // log2(sizeof(T) * std::numeric_limits<char>::digits()) - 1

		for (int i = 0; i < iter_count; ++i)
		{
			std::uint32_t mp = mk ^ (mk << 1);
			mp ^= mp << 2;
			mp ^= mp << 4;
			mp ^= mp << 8;
			mp ^= mp << 16;

			std::uint32_t mv = mp & m;
			m = (m ^ mv) | (mv >> (1 << i));
			std::uint32_t t {op & mv};
			op = (op ^ t) | (t >> (1 << i));
			mk &= ~mp;
		}

		return op;
	};

	inline std::uint32_t expand(const std::uint32_t x, const std::uint32_t y, const std::uint32_t mask) noexcept
	{
		std::uint32_t m {mask}, mk {~mask << 1};
		std::uint32_t op {x};

		constexpr size_t iter_count { 5 }; // log2(sizeof(T) * std::numeric_limits<char>::digits()) - 1

		std::array<std::uint32_t, iter_count> arr;

		for (int i = 0; i < iter_count; ++i)
		{
			std::uint32_t mp = mk ^ (mk << 1);
			mp ^= mp << 2;
			mp ^= mp << 4;
			mp ^= mp << 8;
			mp ^= mp << 16;
			std::uint32_t mv = mp & m;
			arr[i] = mv;

			m = (m ^ mv) | (mv >> (1 << i));
			mk &= ~mp;
		}

		for (int i = (iter_count - 1); i >= 0; --i)
		{
			const auto mv {arr[i]};
			const std::uint32_t t {op << (1 << i)};
			op = (op & ~mv) | (t & mv);
		}

		return (op & mask) | (x & ~mask);
	}

	template<typename DividendType, typename DivisorType> constexpr DividendType int_div_rounded(const DividendType dividend, const DivisorType divisor) noexcept
	{
		return (dividend + divisor / 2) / divisor;
	}

	template<typename T> struct fake_dep : std::false_type { };

	template<typename... Args> inline void pass(Args&&...) { }
}

#endif // EPL_UTIL_H

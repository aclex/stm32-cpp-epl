/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F0XX_USART2_H
#define STM32_CPP_EPL_MCU_STM32F0XX_USART2_H

extern USART_TypeDef hw_usart_2;

namespace stm32
{
	namespace epl
	{
		extern "C" void USART2_IRQHandler() noexcept;

		template<> template<> constexpr USART_TypeDef* peripherals<mcu_model::stm32f0xx>::usart<2>::constexpr_spec_init_helper::handle() noexcept
		{
			return &hw_usart_2;
		}

		template<> template<> constexpr peripherals<mcu_model::stm32f0xx>::gpio::af peripherals<mcu_model::stm32f0xx>::usart<2>::constexpr_spec_init_helper::af() noexcept
		{
			return gpio::af::usart2;
		}

		template<> template<> constexpr peripherals<mcu_model::stm32f0xx>::rcc::bus_address peripherals<mcu_model::stm32f0xx>::usart<2>::constexpr_spec_init_helper::clock_bus_address() noexcept
		{
			return rcc::bus_address::apb1;
		}

		template<> template<> constexpr std::uint32_t peripherals<mcu_model::stm32f0xx>::usart<2>::constexpr_spec_init_helper::clock_flag() noexcept
		{
			return RCC_APB1ENR_USART2EN;
		}

		template<> template<> struct peripherals<mcu_model::stm32f0xx>::usart<2>::constexpr_spec_init_helper::clock_bus : peripherals<mcu_model::stm32f0xx>::rcc::bus<peripherals<mcu_model::stm32f0xx>::usart<2>::constexpr_spec_init_helper::clock_bus_address()> { };

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::usart<2>::signal_mapper<io::usart::pin_signal::rx>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 3:
				case 15:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::usart<2>::signal_mapper<io::usart::pin_signal::tx>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 2:
				case 14:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::usart<2>::signal_mapper<io::usart::pin_signal::rts>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 1:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::usart<2>::signal_mapper<io::usart::pin_signal::cts>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 0:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::usart<2>::signal_mapper<io::usart::pin_signal::ck>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 4:
					return true;
				}
			}

			return false;
		}

		template<> template<> inline void peripherals<mcu_model::stm32f0xx>::usart<2>::set_port_interrupt_handler(const io::usart::interrupt ir, const std::size_t priority, void (* const f)()) noexcept
		{
			s_int_handlers[ir] = f;

			switch_interrupt_source<state::on>(ir);

			NVIC_EnableIRQ(USART2_IRQn);
			NVIC_SetPriority(USART2_IRQn, priority);
		}
	}
}

#endif // STM32_CPP_EPL_MCU_STM32F0XX_USART2_H

/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F0XX_SYS_TICK_H
#define STM32_CPP_EPL_MCU_STM32F0XX_SYS_TICK_H

#include <core_cm0.h>

#include <algorithm>

extern SysTick_Type hw_sys_tick;

namespace stm32
{
	namespace epl
	{
		template<> struct peripherals<mcu_model::stm32f0xx>::sys_tick
		{
			static constexpr SysTick_Type* handle = &hw_sys_tick;

			static std::uint32_t load() noexcept
			{
				return handle->LOAD;
			}

			static void set_load(const std::uint32_t v) noexcept
			{
				handle->LOAD = v;
			}

			static std::uint32_t value() noexcept
			{
				return handle->VAL;
			}

			static void set_value(const std::uint32_t v) noexcept
			{
				handle->VAL = v;
			}

			static bool counted() noexcept
			{
				return handle->CTRL & SysTick_CTRL_COUNTFLAG_Msk;
			}

			static constexpr unsigned char prescaler() noexcept
			{
				return 8;
			}

			static constexpr std::uint32_t max() noexcept
			{
				return 0x00ffffff;
			}

			static void enable() noexcept
			{
				handle->CTRL = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk;
			}

			static void disable() noexcept
			{
				handle->CTRL ^= SysTick_CTRL_ENABLE_Msk;
			}

			static void delay(const typename std::chrono::nanoseconds::rep ticks)
			{
				auto sys_tick_reload_value { std::min(ticks - 1, static_cast<std::chrono::nanoseconds::rep>(max())) };

				set_load(sys_tick_reload_value);
				set_value(0);
				enable();

				while (ticks)
				{
					ticks -= sys_tick_reload_value + 1;

					sys_tick_reload_value = std::min(ticks - 1, static_cast<std::chrono::nanoseconds::rep>(sys_tick_reload_value));
					set_load(sys_tick_reload_value);

					while (!counted());
				}

				disable();
			}

			template<epl::state st>
			static void set_enabled() noexcept
			{
				if constexpr (st == epl::state::off)
				{
					disable();
				}
				else
				{
					enable();
				}
			}
		};
	}
}

#endif // STM32_CPP_EPL_MCU_SMT32F0XX_SYS_TICK_H

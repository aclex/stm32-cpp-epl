/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F0XX_TIM15_H
#define STM32_CPP_EPL_MCU_STM32F0XX_TIM15_H

extern TIM_TypeDef hw_tim_15;

namespace stm32
{
	namespace epl
	{
		template<> template<> constexpr TIM_TypeDef* peripherals<mcu_model::stm32f0xx>::tim<15>::constexpr_spec_init_helper::handle() noexcept
		{
			return &hw_tim_15;
		}

		template<> template<> constexpr peripherals<mcu_model::stm32f0xx>::gpio::af peripherals<mcu_model::stm32f0xx>::tim<15>::constexpr_spec_init_helper::af() noexcept
		{
			return gpio::af::tim15;
		}

		template<> template<> constexpr peripherals<mcu_model::stm32f0xx>::rcc::bus_address peripherals<mcu_model::stm32f0xx>::tim<15>::constexpr_spec_init_helper::clock_bus_address() noexcept
		{
			return rcc::bus_address::apb2;
		}

		template<> template<> constexpr std::uint32_t peripherals<mcu_model::stm32f0xx>::tim<15>::constexpr_spec_init_helper::clock_flag() noexcept
		{
			return RCC_APB2ENR_TIM15EN;
		}

		template<> template<> constexpr std::size_t peripherals<mcu_model::stm32f0xx>::tim<15>::constexpr_spec_init_helper::channel_count() noexcept
		{
			return 2;
		}

		template<> template<> struct peripherals<mcu_model::stm32f0xx>::tim<15>::constexpr_spec_init_helper::clock_bus : peripherals<mcu_model::stm32f0xx>::rcc::bus<peripherals<mcu_model::stm32f0xx>::tim<15>::constexpr_spec_init_helper::clock_bus_address()> { };

		template<> template<> inline epl::state peripherals<mcu_model::stm32f0xx>::tim<15>::one_pulse_mode() noexcept
		{
			return static_cast<epl::state>(tim::handle->CR1 & (1 << 3) >> 3);
		}

		template<> template<> inline void peripherals<mcu_model::stm32f0xx>::tim<15>::set_one_pulse_mode(epl::state st) noexcept
		{
			set_bits(tim::handle->CR1, 0b1 << 3, static_cast<std::uint16_t>(st) << 3);
		}

		template<> template<> inline void peripherals<mcu_model::stm32f0xx>::tim<15>::enable_outputs() noexcept
		{
			tim::handle->BDTR |= TIM_BDTR_MOE;
		}

		template<> template<> inline void peripherals<mcu_model::stm32f0xx>::tim<15>::disable_outputs() noexcept
		{
			tim::handle->BDTR &= ~TIM_BDTR_MOE;
		}

		template<> template<> template<> struct peripherals<mcu_model::stm32f0xx>::tim<15>::af_patcher<timer::pin_signal::ch1n>
		{
			template<typename Patcher>
			static constexpr std::uint8_t patch() noexcept
			{
				return Patcher::template patch<3>();
			}
		};

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::tim<15>::signal_mapper<timer::pin_signal::ch1>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 2:
					return true;
				}

			case gpio::PB:
				switch (pin_no)
				{
				case 14:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::tim<15>::signal_mapper<timer::pin_signal::ch2>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 3:
					return true;
				}

			case gpio::PB:
				switch (pin_no)
				{
				case 15:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::tim<15>::signal_mapper<timer::pin_signal::ch1n>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 1:
					return true;
				}

			case gpio::PB:
				switch (pin_no)
				{
				case 15:
					return true;
				}
			}

			return false;
		}

		template<> template<> template<> constexpr bool peripherals<mcu_model::stm32f0xx>::tim<15>::signal_mapper<timer::pin_signal::bkin>::find_mapping(peripherals<mcu_model::stm32f0xx>::gpio::port_name pn, std::size_t pin_no) noexcept
		{
			switch (pn)
			{
			case gpio::PA:
				switch (pin_no)
				{
				case 9:
					return true;
				}

			case gpio::PB:
				switch (pin_no)
				{
				case 12:
				case 15:
					return true;
				}
			}

			return false;
		}
	}
}

#endif // STM32_CPP_EPL_MCU_STM32F0XX_TIM15_H

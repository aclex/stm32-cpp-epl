	hw_periph_base = 0x40000000

	hw_apb1_periph_base = hw_periph_base
	hw_apb2_periph_base = hw_periph_base + 0x00010000
	hw_ahb1_periph_base = hw_periph_base + 0x00020000

# RCC

	.global hw_rcc
	hw_rcc = hw_ahb1_periph_base + 0x00003800

# GPIO

	.global hw_gpio_a
	hw_gpio_a = hw_ahb1_periph_base + 0x00000000

	.global hw_gpio_b
	hw_gpio_b = hw_ahb1_periph_base + 0x00000400

	.global hw_gpio_c
	hw_gpio_c = hw_ahb1_periph_base + 0x00000800

	.global hw_gpio_d
	hw_gpio_d = hw_ahb1_periph_base + 0x00000c00

	.global hw_gpio_e
	hw_gpio_e = hw_ahb1_periph_base + 0x00001000

	.global hw_gpio_h
	hw_gpio_h = hw_ahb1_periph_base + 0x00001c00

# USART

	.global hw_usart_1
	hw_usart_1 = hw_apb2_periph_base + 0x00001000

	.global hw_usart_2
	hw_usart_2 = hw_apb1_periph_base + 0x00004400

	.global hw_usart_6
	hw_usart_6 = hw_apb2_periph_base + 0x00001400

# SPI

	.global hw_spi_1
	hw_spi_1 = hw_apb2_periph_base + 0x00003000

	.global hw_spi_2
	hw_spi_2 = hw_apb1_periph_base + 0x00003800

	.global hw_spi_3
	hw_spi_3 = hw_apb1_periph_base + 0x00003c00

	.global hw_spi_4
	hw_spi_4 = hw_apb2_periph_base + 0x00003400

	.global hw_spi_5
	hw_spi_5 = hw_apb2_periph_base + 0x00005000

# TIM

	.global hw_tim_1
	hw_tim_1 = hw_apb2_periph_base + 0x00000000

	.global hw_tim_2
	hw_tim_2 = hw_apb1_periph_base + 0x00000000

	.global hw_tim_3
	hw_tim_3 = hw_apb1_periph_base + 0x00000400

	.global hw_tim_4
	hw_tim_4 = hw_apb1_periph_base + 0x00000800

	.global hw_tim_5
	hw_tim_5 = hw_apb1_periph_base + 0x00000c00

	.global hw_tim_9
	hw_tim_9 = hw_apb2_periph_base + 0x00004000

	.global hw_tim_10
	hw_tim_10 = hw_apb2_periph_base + 0x00004400

	.global hw_tim_11
	hw_tim_11 = hw_apb2_periph_base + 0x00004800

# ADC

	.global hw_adc_1
	hw_adc_1 = hw_apb2_periph_base + 0x00002000

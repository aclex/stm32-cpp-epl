/*
  EPL - peripheral library elements for STM32 microcontroller family
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F0XX_DMA4_H
#define STM32_CPP_EPL_MCU_STM32F0XX_DMA4_H

extern DMA_Channel_TypeDef hw_dma_4;

namespace stm32
{
	namespace epl
	{
		template<> template<> constexpr DMA_Channel_TypeDef* peripherals<mcu_model::stm32f0xx>::dma<4>::constexpr_spec_init_helper::handle() noexcept
		{
			return &hw_dma_4;
		}

		template<> template<> constexpr peripherals<mcu_model::stm32f0xx>::rcc::bus_address peripherals<mcu_model::stm32f0xx>::dma<4>::constexpr_spec_init_helper::clock_bus_address() noexcept
		{
			return rcc::bus_address::ahb;
		}

		template<> template<> constexpr std::uint32_t peripherals<mcu_model::stm32f0xx>::dma<4>::constexpr_spec_init_helper::clock_flag() noexcept
		{
			return RCC_AHBENR_DMA1EN;
		}

		template<> template<> struct peripherals<mcu_model::stm32f0xx>::dma<4>::constexpr_spec_init_helper::clock_bus : peripherals<mcu_model::stm32f0xx>::rcc::bus<peripherals<mcu_model::stm32f0xx>::dma<4>::constexpr_spec_init_helper::clock_bus_address()> { };

	}
}

#endif // STM32_CPP_EPL_MCU_STM32F0XX_DMA4_H

/*
  EPL - peripheral library elements for STM32 microcontroller family
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F411XE_H
#define STM32_CPP_EPL_MCU_STM32F411XE_H

#include <cstddef>

#include <stm32f411xe.h>

#include <epl/mcu.h>

namespace stm32
{
	namespace epl
	{
		template<mcu_model m> class peripherals
		{
		public:
			struct rcc;
			struct gpio;
			template<std::size_t no> class usart;
			template<std::size_t no> class spi;
			template<std::size_t no> class adc;
			template<std::size_t no> class tim;
			template<std::size_t no> class dma;
		};

		template<> struct basic_mcu<mcu_model::stm32f411xe> : public peripherals<mcu_model::stm32f411xe>
		{
		};

		using mcu = basic_mcu<mcu_model::stm32f411xe>;
	}
}

// include headers with peripherals specializations to not grow the file too long

#include "stm32f411xe/rcc.h"
#include "stm32f411xe/gpio.h"
#include "stm32f411xe/usart.h"
#include "stm32f411xe/spi.h"
// #include "stm32f411xe/tim.h"
// #include "stm32f411xe/dma.h"
// #include "stm32f411xe/adc.h"

#endif // STM32_CPP_EPL_MCU_STM32F411XE_H

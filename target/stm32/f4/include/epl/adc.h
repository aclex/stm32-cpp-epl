/*
  EPL - peripheral library elements for STM32 microcontroller family
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F0XX_ADC_H
#define STM32_CPP_EPL_MCU_STM32F0XX_ADC_H

#include <epl/state.h>
#include <epl/util.h>

#include <epl/adc/awd_channel_selection.h>
#include <epl/adc/conversion_mode.h>
#include <epl/adc/overrun_mode.h>
#include <epl/adc/external_trigger_mode.h>
#include <epl/adc/data_alignment.h>
#include <epl/adc/scan_direction.h>
#include <epl/adc/clock_mode.h>

namespace stm32
{
	namespace epl
	{
		template<> template<std::size_t no> class peripherals<mcu_model::stm32f0xx>::adc<no>
		{
			struct constexpr_spec_init_helper
			{
				static constexpr ADC_TypeDef* handle() noexcept;
				static constexpr gpio::af af() noexcept;
				static constexpr rcc::bus_address clock_bus_address() noexcept;
				struct clock_bus;
				static constexpr std::uint32_t clock_flag() noexcept;
				static constexpr std::uint32_t reset_command() noexcept;

				static constexpr std::size_t channel_count() noexcept;
			};

		public:
			static constexpr ADC_TypeDef* const handle = constexpr_spec_init_helper::handle();

			static constexpr const gpio::af af = constexpr_spec_init_helper::af();
			static constexpr const rcc::bus_address clock_bus_address = constexpr_spec_init_helper::clock_bus_address();
			using clock_bus = typename constexpr_spec_init_helper::clock_bus;
			static constexpr const std::uint32_t clock_flag = constexpr_spec_init_helper::clock_flag();

			struct channel_mapper
			{
				static constexpr std::size_t get_channel(gpio::port_name pn, std::size_t pin_no) noexcept;

				template<typename ChannelType>
				static constexpr std::size_t get_channel() noexcept;
			};

			template<std::uint16_t channel_suite_value>
			static void set_active_channels() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CHSELR);

				reg = channel_suite_value;
			}

			static void enable() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR);
				constexpr const std::uint32_t mask(ADC_CR_ADEN);

				set_bits(reg, mask, ADC_CR_ADEN);
			}

			static bool enabled() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR);
				constexpr const std::uint32_t mask(ADC_CR_ADEN);

				return reg & mask;
			}

			static void disable() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR);
				constexpr const std::uint32_t mask(ADC_CR_ADDIS);

				set_bits(reg, mask, ADC_CR_ADDIS);
			}

			static void calibrate() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR);
				constexpr const std::uint32_t mask(ADC_CR_ADCAL);

				set_bits(reg, mask, ADC_CR_ADCAL);
			}

			static bool calibrating() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR);
				constexpr const std::uint32_t mask(ADC_CR_ADCAL);

				return reg & mask;
			}

			static void start_conversion() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR);
				constexpr const std::uint32_t mask(ADC_CR_ADSTART);

				set_bits(reg, mask, ADC_CR_ADSTART);
			}

			static bool ongoing_conversion() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR);
				constexpr const std::uint32_t mask(ADC_CR_ADSTART);

				return reg & mask;
			}

			static void stop_conversion() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR);
				constexpr const std::uint32_t mask(ADC_CR_ADSTP);

				set_bits(reg, mask, ADC_CR_ADSTP);
			}

			static bool stopping_conversion() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->CR);
				constexpr const std::uint32_t mask(ADC_CR_ADSTP);

				return reg & mask;
			}

			static std::uint16_t value() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->DR);

				return static_cast<std::uint16_t>(reg);
			}

			static bool ready() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->ISR);
				constexpr const std::uint32_t mask(ADC_ISR_ADRDY);

				return reg & mask;
			}

			static void clear_ready() noexcept
			{
				constexpr volatile std::uint32_t& reg(handle->ISR);
				constexpr const std::uint32_t mask(ADC_ISR_ADRDY);

				set_bits(reg, mask, ADC_ISR_ADRDY);
			}

			template<std::size_t channel_no> static void select_for_analog_watchdog() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CFGR1);
				constexpr const std::uint16_t mask(ADC_CFGR1_AWDCH);
				constexpr const std::size_t shift_amount(26);

				set_bits(reg, mask, static_cast<std::uint16_t>(channel_no) << shift_amount);
			}

			template<state st> static void switch_analog_watchdog() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CFGR1);
				constexpr const std::uint16_t mask(ADC_CFGR1_AWDEN);

				set_bits(reg, mask, static_cast<bool>(st) ? ADC_CFGR1_AWDEN : 0);
			}

			template<epl::adc::awd_channel_selection aws>
			static void set_analog_watchdog_channel_selection() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CFGR1);
				constexpr const std::uint16_t mask(ADC_CFGR1_AWDSGL);

				set_bits(reg, mask, awd_channel_selection_value<aws>());
			}

			template<epl::adc::conversion_mode m> static void set_mode() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CFGR1);
				constexpr const std::uint16_t mask(ADC_CFGR1_DISCEN | ADC_CFGR1_CONT);

				set_bits(reg, mask, mode_value<m>());
			}

			template<state st> static void switch_auto_off() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CFGR1);
				constexpr const std::uint16_t mask(ADC_CFGR1_AUTOFF);

				set_bits(reg, mask, static_cast<bool>(st) ? ADC_CFGR1_AUTOFF : 0);
			}

			template<state st> static void switch_wait() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CFGR1);
				constexpr const std::uint16_t mask(ADC_CFGR1_WAIT);

				set_bits(reg, mask, static_cast<bool>(st) ? ADC_CFGR1_WAIT : 0);
			}

			template<epl::adc::overrun_mode ovm> static void set_overrun_management_mode() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CFGR1);
				constexpr const std::uint16_t mask(ADC_CFGR1_WAIT);

				set_bits(reg, mask, overrun_management_mode_value<ovm>());
			}

			template<epl::adc::external_trigger_mode ext, std::size_t trigger_source>
			static void configure_external_trigger() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CFGR1);
				constexpr const std::uint16_t mask_mode(ADC_CFGR1_EXTEN);
				constexpr const std::size_t shift_amount_mode(10);

				set_bits(reg, mask_mode, external_trigger_mode_value<ext>() << shift_amount_mode);

				if constexpr (ext != epl::adc::external_trigger_mode::disabled)
				{
					static_assert(trigger_source <= 7, "Trigger source number should be in range [0, 7].");

					constexpr const std::uint16_t mask_source(ADC_CFGR1_EXTSEL);
					constexpr const std::size_t shift_amount_source(6);

					set_bits(reg, mask_source, static_cast<std::uint16_t>(trigger_source) << shift_amount_source);
				}
			}

			template<epl::adc::data_alignment al> static void set_data_alignment() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CFGR1);
				constexpr const std::uint16_t mask(ADC_CFGR1_ALIGN);
				constexpr const std::size_t shift_amount(6);

				set_bits(reg, mask, data_alignment_value<al>() << shift_amount);
			}

			static std::size_t data_resolution() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CFGR1);
				constexpr const std::uint16_t mask(ADC_CFGR1_RES);
				constexpr const std::size_t shift_amount(3);

				return decode_data_resolution_value((reg & mask) >> shift_amount);
			}

			template<std::size_t res> static void set_data_resolution() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CFGR1);
				constexpr const std::uint16_t mask(ADC_CFGR1_RES);
				constexpr const std::size_t shift_amount(3);

				set_bits(reg, mask, data_resolution_value<res>() << shift_amount);
			}

			template<epl::adc::scan_direction dir> static void set_scan_direction() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CFGR1);
				constexpr const std::uint16_t mask(ADC_CFGR1_SCANDIR);

				set_bits(reg, mask, scan_direction_value<dir>());
			}

			template<epl::adc::clock_mode clkm> static void set_clock_mode() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CFGR2);
				constexpr const std::uint16_t mask(ADC_CFGR2_CKMODE);
				constexpr const std::size_t shift_amount(30);

				set_bits(reg, mask, clock_mode_value<clkm>() << shift_amount);
			}

			static void set_sampling_time(const float sampling_time) noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->CFGR2);
				constexpr const std::uint16_t mask(ADC_CFGR2_CKMODE);
				constexpr const std::size_t shift_amount(0);

				set_bits(reg, mask, sampling_time_value(sampling_time) << shift_amount);
			}

			template<std::uint16_t low, std::uint16_t high>
			static void set_analog_watchdog_thresholds() noexcept
			{
				constexpr volatile std::uint16_t& reg(handle->TR);

				reg = (high << 16) | low;
			}

	private:
			template<epl::adc::awd_channel_selection aws>
			static constexpr std::uint16_t awd_channel_selection_value() noexcept
			{

				switch (aws)
				{
				case epl::adc::awd_channel_selection::all:
					return 0;

				case epl::adc::awd_channel_selection::single:
					return ADC_CFGR1_AWDSGL;
				}
			}

			template<epl::adc::conversion_mode m>
			static constexpr std::uint16_t mode_value() noexcept
			{
				switch (m)
				{
				case epl::adc::conversion_mode::discontinuous:
					return ADC_CFGR1_DISCEN;

				case epl::adc::conversion_mode::single:
					return 0;

				case epl::adc::conversion_mode::continuous:
					return ADC_CFGR1_CONT;
				}
			}

			template<epl::adc::overrun_mode ovm>
			static constexpr std::uint16_t overrun_management_mode_value() noexcept
			{
				switch (ovm)
				{
				case epl::adc::overrun_mode::overwrite:
					return ADC_CFGR1_OVRMOD;

				case epl::adc::overrun_mode::preserve:
					return 0;
				}
			}

			template<epl::adc::external_trigger_mode ext>
			static constexpr std::uint16_t external_trigger_mode_value() noexcept
			{
				switch (ext)
				{
				case epl::adc::external_trigger_mode::disabled:
					return 0;

				case epl::adc::external_trigger_mode::rising_edge:
					return 1;

				case epl::adc::external_trigger_mode::falling_edge:
					return 2;

				case epl::adc::external_trigger_mode::both_edges:
					return 3;
				}
			}

			template<epl::adc::data_alignment al>
			static constexpr std::uint16_t data_alignment_value() noexcept
			{
				switch (al)
				{
				case epl::adc::data_alignment::right:
					return 0;

				case epl::adc::data_alignment::left:
					return 1;
				}
			}

			static constexpr std::size_t decode_data_resolution_value(const std::uint16_t dv) noexcept
			{
				switch (dv)
				{
				default:
				case 0:
					return 12;

				case 1:
					return 10;

				case 2:
					return 8;

				case 3:
					return 6;
				}
			}

			template<std::size_t res>
			static constexpr std::uint16_t data_resolution_value() noexcept
			{
				constexpr const epl::constexpr_map<std::size_t, std::uint16_t, 4> res_map(
				{{
					{ 6, 3 },
					{ 8, 2 },
					{ 10, 1 },
					{ 12, 0 }
				}});

				static_assert(res_map.count(res), "Unsupported resolution value: should be one of 6, 8, 10 or 12.");

				return res_map.at(res);
			}

			template<epl::adc::scan_direction dir>
			static constexpr std::uint16_t scan_direction_value() noexcept
			{
				switch (dir)
				{
				case epl::adc::scan_direction::forward:
					return 0;

				case epl::adc::scan_direction::backward:
					return ADC_CFGR1_SCANDIR;
				}
			}

			template<epl::adc::clock_mode clkm>
			static constexpr std::uint16_t clock_mode_value() noexcept
			{
				switch (clkm)
				{
				case epl::adc::clock_mode::adc_clock:
					return 0;

				case epl::adc::clock_mode::pclk_div_2:
					return 1;

				case epl::adc::clock_mode::pclk_div_4:
					return 2;
				}
			}

			static constexpr std::uint16_t sampling_time_value(const float sampling_time) noexcept
			{
				constexpr const epl::constexpr_map<float, std::uint16_t, 8> sampling_time_map(
				{{
					{ 1.5, 0 },
					{ 7.5, 1 },
					{ 13.5, 2 },
					{ 28.5, 3 },
					{ 41.5, 4 },
					{ 55.5, 5 },
					{ 71.5, 6 },
					{ 239.5, 7 }
				}});

				// static_assert(sampling_time_map.count(sampling_time), "Unsupported value of sampling time: should be one of 1.5, 7.5, 13.5, 28.5, 41.5, 55.5, 71.5, 239.5.");

				return sampling_time_map.at(sampling_time);
			}
		};
	}
}

#include "adc1.h"

#endif // STM32_CPP_EPL_MCU_STM32F0XX_ADC_H

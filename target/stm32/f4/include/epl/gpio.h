/*
  EPL - peripheral library elements for STM32 microcontroller family
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STM32_CPP_EPL_MCU_STM32F411XE_GPIO_H
#define STM32_CPP_EPL_MCU_STM32F411XE_GPIO_H

#include <epl/util.h>

extern GPIO_TypeDef hw_gpio_a;
extern GPIO_TypeDef hw_gpio_b;
extern GPIO_TypeDef hw_gpio_c;
extern GPIO_TypeDef hw_gpio_d;
extern GPIO_TypeDef hw_gpio_e;
extern GPIO_TypeDef hw_gpio_h;

namespace stm32
{
	namespace epl
	{
		template<> struct peripherals<mcu_model::stm32f411xe>::gpio
		{
			enum port_name : unsigned char
			{
				PA,
				PB,
				PC,
				PD,
				PE,
				PH
			};

			template<port_name pn> struct port;

			enum class speed : unsigned char
			{
				low = 0x0, // 2 MHz
				medium = 0x1, // 10 MHz
				high = 0x3 // 50 MHz
			};

			enum class af : unsigned char
			{
				gpio,
				tim1,
				tim3,
				tim4,
				tim5,
				tim9,
				tim10,
				tim11,
				i2c1,
				i2c2,
				i2c3,
				spi1,
				spi2,
				spi3,
				spi4,
				spi5,
				usart1,
				usart2,
				usart6,
				usb,
				sdio,
				eventout,
				mco,
				swdio,
				swclk
			};

			template<af f> struct af_mapper
			{
				static constexpr const std::uint8_t s_not_found_value = 0xff;
				template<port_name pn, std::size_t pin_no>
				static constexpr std::uint8_t value()
				{
					constexpr std::uint8_t result(find_mapping(pn, pin_no));
					static_assert(result != s_not_found_value,
						"No requested alternate function found for the specified pin.");
					return result;
				}

				static constexpr std::uint8_t find_mapping(port_name pn, std::size_t pin_no)
				{
					return s_not_found_value;
				}
			};
		};

		template<> template<> struct peripherals<mcu_model::stm32f411xe>::gpio::port<peripherals<mcu_model::stm32f411xe>::gpio::PA>
		{
			static constexpr GPIO_TypeDef* handle = &hw_gpio_a;
			static constexpr rcc::bus_address clock_bus_address = rcc::bus_address::ahb1;
			typedef rcc::bus<clock_bus_address> clock_bus;
			static constexpr std::uint32_t clock_flag = RCC_AHB1ENR_GPIOAEN;
		};

		template<> template<> struct peripherals<mcu_model::stm32f411xe>::gpio::port<peripherals<mcu_model::stm32f411xe>::gpio::PB>
		{
			static constexpr GPIO_TypeDef* handle = &hw_gpio_b;
			static constexpr rcc::bus_address clock_bus_address = rcc::bus_address::ahb1;
			typedef rcc::bus<clock_bus_address> clock_bus;
			static constexpr std::uint32_t clock_flag = RCC_AHB1ENR_GPIOBEN;
		};

		template<> template<> struct peripherals<mcu_model::stm32f411xe>::gpio::port<peripherals<mcu_model::stm32f411xe>::gpio::PC>
		{
			static constexpr GPIO_TypeDef* handle = &hw_gpio_c;
			static constexpr rcc::bus_address clock_bus_address = rcc::bus_address::ahb1;
			typedef rcc::bus<clock_bus_address> clock_bus;
			static constexpr std::uint32_t clock_flag = RCC_AHB1ENR_GPIOCEN;
		};

		template<> template<> struct peripherals<mcu_model::stm32f411xe>::gpio::port<peripherals<mcu_model::stm32f411xe>::gpio::PD>
		{
			static constexpr GPIO_TypeDef* handle = &hw_gpio_d;
			static constexpr rcc::bus_address clock_bus_address = rcc::bus_address::ahb1;
			typedef rcc::bus<clock_bus_address> clock_bus;
			static constexpr std::uint32_t clock_flag = RCC_AHB1ENR_GPIODEN;
		};

		template<> template<> struct peripherals<mcu_model::stm32f411xe>::gpio::port<peripherals<mcu_model::stm32f411xe>::gpio::PE>
		{
			static constexpr GPIO_TypeDef* handle = &hw_gpio_e;
			static constexpr rcc::bus_address clock_bus_address = rcc::bus_address::ahb1;
			typedef rcc::bus<clock_bus_address> clock_bus;
			static constexpr std::uint32_t clock_flag = RCC_AHB1ENR_GPIOEEN;
		};

		template<> template<> struct peripherals<mcu_model::stm32f411xe>::gpio::port<peripherals<mcu_model::stm32f411xe>::gpio::PH>
		{
			static constexpr GPIO_TypeDef* handle = &hw_gpio_h;
			static constexpr rcc::bus_address clock_bus_address = rcc::bus_address::ahb1;
			typedef rcc::bus<clock_bus_address> clock_bus;
			static constexpr std::uint32_t clock_flag = RCC_AHB1ENR_GPIOHEN;
		};

		// GPIO alternative functions

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::tim1>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
				case PA:
					switch (pin_no)
					{
					case 6:
					case 7:
					case 8:
					case 9:
					case 10:
					case 11:
					case 12:
						return 1;
					}

				case PB:
					switch (pin_no)
					{
					case 0:
					case 1:
					case 12:
					case 13:
					case 14:
					case 15:
						return 1;
					}

				case PE:
					switch (pin_no)
					{
					case 7:
					case 8:
					case 9:
					case 10:
					case 11:
					case 12:
					case 13:
					case 14:
					case 15:
						return 1;
					}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::tim3>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 6:
				case 7:
					return 2;
				}

			case PB:
				switch (pin_no)
				{
				case 0:
				case 1:
				case 4:
				case 5:
					return 2;
				}

			case PC:
				switch (pin_no)
				{
				case 6:
				case 7:
				case 8:
				case 9:
					return 2;
				}

			case PD:
				switch (pin_no)
				{
				case 2:
					return 2;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::tim4>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PB:
				switch (pin_no)
				{
				case 6:
				case 7:
				case 8:
				case 9:
					return 2;
				}

			case PD:
				switch (pin_no)
				{
				case 12:
				case 13:
				case 14:
				case 15:
					return 2;
				}

			case PE:
				switch (pin_no)
				{
				case 0:
					return 2;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::tim5>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 0:
				case 1:
				case 2:
				case 3:
					return 2;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::tim9>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 2:
				case 3:
					return 3;
				}

			case PE:
				switch (pin_no)
				{
				case 5:
				case 6:
					return 3;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::tim10>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PB:
				switch (pin_no)
				{
				case 8:
					return 3;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::tim11>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PB:
				switch (pin_no)
				{
				case 9:
					return 3;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::i2c1>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PB:
				switch (pin_no)
				{
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
					return 4;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::i2c2>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PB:
				switch (pin_no)
				{
				case 10:
				case 11:
				case 12:
					return 4;

				case 3:
				case 9:
					return 9;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::i2c3>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 8:
				case 9:
					return 4;
				}

			case PB:
				switch (pin_no)
				{
				case 4:
				case 8:
					return 9;
				}

			case PC:
				switch (pin_no)
				{
				case 9:
					return 4;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::spi1>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 4:
				case 5:
				case 6:
				case 7:
				case 15:
					return 5;
				}

			case PB:
				switch (pin_no)
				{
				case 3:
				case 4:
				case 5:
					return 5;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::spi2>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PB:
				switch (pin_no)
				{
				case 9:
				case 10:
					return 5;

				case 12:
				case 13:
				case 14:
				case 15:
					return 5;
				}

			case PC:
				switch (pin_no)
				{
				case 2:
				case 3:
				case 7:
					return 5;
				}

			case PD:
				switch (pin_no)
				{
				case 3:
					return 5;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::spi3>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 4:
				case 15:
					return 6;
				}

			case PB:
				switch (pin_no)
				{
				case 3:
				case 4:
				case 5:
					return 6;

				case 12:
					return 7;
				}

			case PC:
				switch (pin_no)
				{
				case 10:
				case 11:
				case 12:
					return 6;
				}

			case PD:
				switch (pin_no)
				{
				case 6:
					return 5;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::spi4>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 1:
					return 5;

				case 11:
					return 6;
				}

			case PB:
				switch (pin_no)
				{
				case 12:
				case 13:
					return 6;
				}

			case PE:
				switch (pin_no)
				{
				case 2:
				case 4:
				case 5:
				case 6:
				case 11:
				case 12:
				case 13:
				case 14:
					return 5;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::spi5>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 10:
				case 12:
					return 6;
				}

			case PB:
				switch (pin_no)
				{
				case 0:
				case 1:
				case 8:
					return 6;
				}

			case PE:
				switch (pin_no)
				{
				case 2:
				case 4:
				case 5:
				case 6:
				case 11:
				case 12:
				case 13:
				case 14:
					return 6;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::usart1>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 8:
				case 9:
				case 10:
				case 11:
				case 12:
				case 15:
					return 7;
				}

			case PB:
				switch (pin_no)
				{
				case 3:
				case 6:
				case 7:
					return 7;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::usart2>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
					return 7;
				}

			case PD:
				switch (pin_no)
				{
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
					return 7;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::usart6>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 11:
				case 12:
					return 8;
				}

			case PC:
				switch (pin_no)
				{
				case 6:
				case 7:
				case 8:
					return 8;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::usb>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 8:
				case 9:
				case 10:
				case 11:
				case 12:
					return 10;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::sdio>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 6:
				case 8:
				case 9:
					return 12;
				}

			case PB:
				switch (pin_no)
				{
				case 4:
				case 5:
				case 7:
				case 8:
				case 9:
				case 10:
				case 14:
				case 15:
					return 12;
				}

			case PC:
				switch (pin_no)
				{
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
				case 11:
				case 12:
					return 12;
				}

			case PD:
				switch (pin_no)
				{
				case 2:
					return 12;
				}

			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::eventout>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				return 15;

			case PB:
				return 15;

			case PC:
				if (pin_no < 13)
				{
					return 15;
				}

			case PD:
				return 15;

			case PE:
				return 15;
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::mco>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 8:
					return 0;
				}

			case PC:
				switch (pin_no)
				{
				case 9:
					return 0;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::swdio>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 13:
					return 0;
				}
			}

			return s_not_found_value;
		}

		template<> constexpr std::uint8_t peripherals<mcu_model::stm32f411xe>::gpio::af_mapper<peripherals<mcu_model::stm32f411xe>::gpio::af::swclk>::find_mapping(peripherals<mcu_model::stm32f411xe>::gpio::port_name pn, std::size_t pin_no)
		{
			switch (pn)
			{
			case PA:
				switch (pin_no)
				{
				case 14:
					return 0;
				}
			}

			return s_not_found_value;
		}
	}
}

#endif // STM32_CPP_EPL_MCU_STM32F411XE_GPIO_H

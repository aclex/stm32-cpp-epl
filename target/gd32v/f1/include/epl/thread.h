/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef GD32VF103XX_EPL_THREAD_H
#define GD32VF103XX_EPL_THREAD_H 

#include <chrono>

#include <epl/rcu.h>
#include <epl/mcu/register_map.h>
#include <epl/chrono.h>
#include <epl/irq.h>

namespace epl::this_thread
{
	template<class Rep, class Period>
	void sleep_for(const std::chrono::duration<Rep, Period>& sleep_duration) noexcept
	{
		using namespace chrono;

		static_assert(std::ratio_greater_equal<Period, system_clock::period>::value, "Can't process periods with better, than MCU clock accuracy.");

		auto current{system_clock::now()};
		const auto done{current + sleep_duration};

		while (current < done)
		{
			irq::unit::wait_for_interrupt();
			current = system_clock::now();
		}
	}
}

#endif // GD32VF103XX_EPL_THREAD_H 

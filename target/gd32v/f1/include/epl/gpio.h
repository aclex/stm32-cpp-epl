/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef GD32VF103XX_EPL_GPIO_H
#define GD32VF103XX_EPL_GPIO_H

#include <memory>

#include <epl/state.h>

#include <epl/rcu.h>

#include <epl/gpio/core.h>
#include <epl/gpio/mode.h>
#include <epl/gpio/pull.h>
#include <epl/gpio/output_mode.h>

#include <epl/gpio/port.h>
#include <epl/gpio/speed.h>

#include <epl/irq.h>

namespace epl::gpio
{
	template<port p> constexpr std::uint32_t clock_flag{};
	template<> constexpr std::uint32_t clock_flag<port::a>{0x04};
	template<> constexpr std::uint32_t clock_flag<port::b>{0x08};
	template<> constexpr std::uint32_t clock_flag<port::c>{0x0f};
	template<> constexpr std::uint32_t clock_flag<port::d>{0x20};
	template<> constexpr std::uint32_t clock_flag<port::e>{0x40};

	constexpr std::uint32_t clock_flag_af{0x01};

	template<port P, std::size_t Number> class pin;

	namespace irq
	{
		template<std::size_t Number> constexpr unsigned int cause;
		template<> constexpr unsigned int cause<0>{25};
		template<> constexpr unsigned int cause<1>{26};
		template<> constexpr unsigned int cause<2>{27};
		template<> constexpr unsigned int cause<3>{28};
		template<> constexpr unsigned int cause<4>{29};
		template<> constexpr unsigned int cause<5>{42};
		template<> constexpr unsigned int cause<6>{42};
		template<> constexpr unsigned int cause<7>{42};
		template<> constexpr unsigned int cause<8>{42};
		template<> constexpr unsigned int cause<9>{42};
		template<> constexpr unsigned int cause<10>{59};
		template<> constexpr unsigned int cause<11>{59};
		template<> constexpr unsigned int cause<12>{59};
		template<> constexpr unsigned int cause<13>{59};
		template<> constexpr unsigned int cause<14>{59};
		template<> constexpr unsigned int cause<15>{59};

		enum class edge : unsigned char
		{
			rising,
			falling,
			both
		};

		template<std::size_t Number> class basic_channel_handler
		{
		public:
			static basic_channel_handler* instance() noexcept
			{
				return s_instance.get();
			}

			virtual void on_event() noexcept {}

			inline void resolve() noexcept
			{
				on_event();
			}

			virtual ~basic_channel_handler() {}

		private:
			template<port, std::size_t> friend class epl::gpio::pin;

			static void set_instance(std::unique_ptr<basic_channel_handler>&& s) noexcept
			{
				s_instance = std::move(s);
			}

			template<class T, typename... Args> static void emplace_instance(Args&&... args) noexcept(noexcept(T(std::forward<Args>(args)...)))
			{
				s_instance = std::make_unique<T>(std::forward<Args>(args)...);
			}

			static inline std::unique_ptr<basic_channel_handler> s_instance;
		};
	}

	template<port P, std::size_t Number> class pin
	{
		static constexpr std::uint32_t mask{1 << Number};

	public:
		static constexpr epl::gpio::port port{P};
		static constexpr std::size_t number{Number};

		using basic_irq_handler_type = irq::basic_channel_handler<number>;

		class irq
		{
		public:
			static inline bool pending() noexcept
			{
				return get_bit(mcu::register_map::exti::pd, s_irq_flag_mask);
			}

			static inline void clear() noexcept
			{
				set_bit(mcu::register_map::exti::pd, s_irq_flag_mask);
			}

			template<epl::gpio::irq::edge e> static inline void enable_handling(std::unique_ptr<basic_irq_handler_type>&& h) noexcept
			{
				basic_irq_handler_type::set_instance(std::move(h));

				disable();
				reset();

				enable<e>();
			}

			template<epl::gpio::irq::edge e, class T, class... Args> static inline void emplace_handling(Args&&... args) noexcept
			{
				basic_irq_handler_type::template emplace_instance<T>(std::forward<Args>(args)...);

				disable();
				reset();

				enable<e>();
			}

			static inline void disable_handling() noexcept
			{
				disable();
				reset();

				basic_irq_handler_type::set_instance(nullptr);
			}

		private:
			template<epl::gpio::irq::edge e> static inline void enable() noexcept
			{
				constexpr auto bus{mcu::register_map::gpio::port_bus<port>};
				rcu::enable_clock<bus, clock_flag<port>>();

				epl::irq::unit::configure(s_irq_cause);
				epl::irq::unit::enable(s_irq_cause);

				{
					constexpr auto offset{4 * (number % 4)};
					constexpr std::uint32_t mask{0x0f << offset};
					constexpr std::uint32_t value{static_cast<std::uint32_t>(port) << offset};
					auto* const reg{mcu::register_map::afio::extiss<number>};

					init_bit(reg, mask, value);
				}

				{
					auto* const reg{mcu::register_map::exti::inten};
					set_bit(reg, s_irq_flag_mask);
				}

				if constexpr (e == epl::gpio::irq::edge::rising)
				{
					set_bit(mcu::register_map::exti::rten, s_irq_flag_mask);
					reset_bit(mcu::register_map::exti::ften, s_irq_flag_mask);
				}

				if constexpr (e == epl::gpio::irq::edge::falling)
				{
					reset_bit(mcu::register_map::exti::rten, s_irq_flag_mask);
					set_bit(mcu::register_map::exti::ften, s_irq_flag_mask);
				}

				if constexpr (e == epl::gpio::irq::edge::both)
				{
					set_bit(mcu::register_map::exti::rten, s_irq_flag_mask);
					set_bit(mcu::register_map::exti::ften, s_irq_flag_mask);
				}

				clear();
			}

			static inline void disable() noexcept
			{
				{
					constexpr auto offset{4 * (number % 4)};
					constexpr std::uint32_t mask{0x0f << offset};
					auto* const reg{mcu::register_map::afio::extiss<number>};

					reset_bit(reg, mask);
				}

				{
					auto* const reg{mcu::register_map::exti::inten};
					reset_bit(reg, s_irq_flag_mask);
				}
			}

			static inline void reset() noexcept
			{
				reset_bit(mcu::register_map::exti::rten, s_irq_flag_mask);
				reset_bit(mcu::register_map::exti::ften, s_irq_flag_mask);
				set_bit(mcu::register_map::exti::pd, s_irq_flag_mask);
			}

			static constexpr auto s_irq_cause{gpio::irq::cause<number>};
			static constexpr std::uint32_t s_irq_flag_mask{1 << number};
		};

		static constexpr void configure(const mode m = mode::analog, const pull pl = pull::none)
		{
			set_mode(m);
			set_pull(pl);
		}

		static constexpr void configure(const output_mode om, const speed sp = {}, const mode m = mode::output)
		{
			set_mode(m);
			set_output_mode(om);
			set_speed(sp);
		}

		static constexpr void toggle() noexcept
		{
			set(!value());
		}

		static constexpr void set(const bool v = true)
		{
			if (v)
			{
				set_bit(mcu::register_map::gpio::bop<port>, mask);
			}
			else
			{
				set_bit(mcu::register_map::gpio::bc<port>, mask);
			}
		}

		static constexpr void reset(const bool v = false)
		{
			pin::set(!v);
		}

		static constexpr void set(const epl::state v)
		{
			pin::set(static_cast<bool>(v));
		}

		static constexpr bool value() noexcept
		{
			return *mcu::register_map::gpio::istat<port> & mask;
		}

		static constexpr epl::state state() noexcept
		{
			return static_cast<epl::state>(pin::value());
		}

	private:
		static constexpr volatile std::uint32_t& get_ctl() noexcept
		{
			return number < 8 ? *mcu::register_map::gpio::ctl0<port> : *mcu::register_map::gpio::ctl1<port>;
		}

		static constexpr std::uint32_t ctl_shift_amount() noexcept
		{
			return number < 8 ? number * 4 : (number - 8) * 4;
		}

		static constexpr std::uint32_t ctl_mask(const std::uint32_t bits) noexcept
		{
			return static_cast<std::uint32_t>(bits << ctl_shift_amount());
		}

		static constexpr void set_mode(const mode m = mode::analog)
		{
			using epl::gpio::mode;

			if (m != mode::analog)
			{
				constexpr auto bus{mcu::register_map::gpio::port_bus<port>};
				rcu::enable_clock<bus, clock_flag<port>>();
			}

			// set at least low speed to switch to anything but the analog or input mode
			if (m == mode::input || m == mode::analog)
			{
				constexpr auto v{~ctl_mask(0b0011)};
				get_ctl() = get_ctl() & v;
			}
			else if (m == mode::af)
			{
				constexpr auto v{ctl_mask(0b1000)};
				get_ctl() = get_ctl() | v;
			}
		}

		static constexpr void set_pull(const pull pl)
		{
			using epl::gpio::pull;

			constexpr auto vc{~ctl_mask(0b1100)};
			get_ctl() = get_ctl() & vc;
			if (pl == pull::none)
			{
				constexpr auto v{ctl_mask(0b0100)};
				get_ctl() = get_ctl() | v;
			}
			else
			{
				constexpr auto v{ctl_mask(0b1000)};
				get_ctl() = get_ctl() | v;

				if (pl == pull::down)
					reset();
				else
					set();
			}
		}

		static constexpr void set_output_mode(const output_mode om)
		{
			using epl::gpio::output_mode;

			if (om == output_mode::push_pull)
			{
				constexpr auto v {~ctl_mask(0b0100)};
				get_ctl() = get_ctl() & v;
			}
			else
			{
				constexpr auto v {ctl_mask(0b0100)};
				get_ctl() = get_ctl() | v;
			}
		}

		static constexpr std::uint32_t speed_to_bits(const speed sp)
		{
			switch (sp)
			{
			default:
			case speed::low:
				return 0b10;

			case speed::medium:
				return 0b01;

			case speed::high:
				return 0b11;
			}
		}

		static constexpr void set_speed(const speed sp)
		{
			constexpr auto vc{~ctl_mask(0b0011)};
			get_ctl() = get_ctl() & vc;

			const auto v{ctl_mask(speed_to_bits(sp))};
			get_ctl() = get_ctl() | v;
		}
	};
}

#endif // GD32VF103XX_EPL_GPIO_H

/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#ifndef GD32VF103XX_EPL_SPI_H
#define GD32VF103XX_EPL_SPI_H

#include <stdexcept>
#include <type_traits>

#include <epl/duplex.h>
#include <epl/bit_ops.h>

#include <epl/gpio.h>

#include <epl/dma/request_width.h>

#include <epl/irq.h>

#include <epl/spi/mode.h>
#include <epl/spi/bit_order.h>

#include <epl/spi/word_length.h>
#include <epl/spi/nss_mode.h>
#include <epl/spi/prescaler.h>

namespace epl::spi
{
	template<std::size_t N, class... Pins> class port;

	template<std::size_t n> static constexpr std::uint32_t port_flag_offset{};
	template<> constexpr std::uint32_t port_flag_offset<0>{12};
	template<> constexpr std::uint32_t port_flag_offset<1>{14};
	template<> constexpr std::uint32_t port_flag_offset<2>{15};

	template<std::size_t n> static constexpr std::uint32_t clock_flag{1 << port_flag_offset<n>};
	template<std::size_t n> static constexpr std::uint32_t reset_flag{1 << port_flag_offset<n>};

	enum class error : unsigned char
	{
		no,
		format,
		rx_overrun,
		tx_underrun,
		configuration,
		crc
	};

	namespace dma
	{
		enum class target
		{
			data
		};

		constinit const target default_target{target::data};

		template<class If, target t = target::data> struct slot
		{
			static epl::dma::request_width width()
			{
				switch (If::word_length())
				{
				default:
				case word_length::b8:
					return epl::dma::request_width::b8;

				case word_length::b16:
					return epl::dma::request_width::b16;
				}
			}

			static constexpr volatile std::uint32_t* address()
			{
				return If::template address<t>();
			}

			template<direction d> static constexpr void enable()
			{
				static_assert(d != direction::na, "Direction should be specified.");

				return If::template enable_dma<t, d>();
			}

			template<direction d> static constexpr void disable()
			{
				static_assert(d != direction::na, "Direction should be specified.");

				return If::template disable_dma<t, d>();
			}
		};
	}

	namespace irq
	{
		template<std::size_t N> inline unsigned int cause{54 + N};
		template<> inline unsigned int cause<2>{70};

		namespace event
		{
			struct tx_empty {};
			struct rx_not_empty {};
		}

		template<std::size_t N> class basic_port_handler
		{
		public:
			static basic_port_handler* instance() noexcept
			{
				return s_instance;
			}

			virtual void on_event(const event::tx_empty& e) noexcept {}
			virtual void on_event(const event::rx_not_empty& e) noexcept {}
			virtual void on_error(const error e) noexcept {}

			void resolve() noexcept
			{
				const auto status{*mcu::register_map::spi::stat<N>};

				if (status & s_conferr_mask)
					on_error(error::configuration);

				if (status & s_ferr_mask)
					on_error(error::format);

				if (status & s_crcerr_mask)
					on_error(error::crc);

				if (status & s_rxorerr_mask)
					on_error(error::rx_overrun);

				if (status & s_txurerr_mask)
					on_error(error::tx_underrun);

				if (status & s_rbne_mask)
					on_event(event::rx_not_empty{});

				if (status & s_tbe_mask)
					on_event(event::tx_empty{});
			}
			virtual ~basic_port_handler() {}

		private:
			template<std::size_t, class...> friend class epl::spi::port;

			static void set_instance(basic_port_handler* const s) noexcept
			{
				s_instance = s;
			}

			static inline basic_port_handler* s_instance{};

			static constexpr std::uint32_t s_ferr_mask{1 << 8};
			static constexpr std::uint32_t s_rxorerr_mask{1 << 6};
			static constexpr std::uint32_t s_conferr_mask{1 << 5};
			static constexpr std::uint32_t s_crcerr_mask{1 << 4};
			static constexpr std::uint32_t s_txurerr_mask{1 << 3};

			static constexpr std::uint32_t s_tbe_mask{1 << 1};
			static constexpr std::uint32_t s_rbne_mask{1};
		};
	}

	template<std::size_t N, class... Pins> class pin_settings
	{
		template<std::size_t, typename...> struct fake_dep : std::false_type { };
		static_assert(fake_dep<N, Pins...>::value, "Incorrect pin configuration for the specified SPI device");

	public:
		static constexpr void configure() noexcept
		{

		}
	};

	template<> class pin_settings<0, gpio::pin<gpio::port::a, 5>, gpio::pin<gpio::port::a, 6>, gpio::pin<gpio::port::a, 7>, gpio::pin<gpio::port::a, 4>>
	{
	public:
		static constexpr void configure() noexcept
		{
			enable_clock();

			gpio::configure<gpio::pin<gpio::port::a, 5>, gpio::pin<gpio::port::a, 6>, gpio::pin<gpio::port::a, 7>, gpio::pin<gpio::port::a, 4>>(gpio::output_mode::push_pull, gpio::speed::high, gpio::mode::af);
		}

	private:
		static constexpr void enable_clock() noexcept
		{
			rcu::enable_clock<mcu::bus::apb2, 1>();

			constexpr auto b{mcu::register_map::gpio::port_bus<gpio::port::a>};
			rcu::enable_clock<b, gpio::clock_flag<gpio::port::a>>();
		}
	};

	template<std::size_t N, class... Pins> class port
	{
		using pin_settings_type = pin_settings<N, Pins...>;

	public:
		static constinit const auto n{N};

		using basic_irq_handler_type = irq::basic_port_handler<N>;

		static constexpr void configure(const mode m, const duplex d, const word_length l, const prescaler p, const bit_order o, const nss_mode nm = nss_mode::disabled) noexcept
		{
			pin_settings_type::configure();

			reset();

			enable_clock();

			set_mode(m);
			set_duplex(d);
			set_word_length(l);
			set_prescaler(p);
			set_bit_order(o);
			set_nss_mode(nm);
		}

		static constexpr void enable() noexcept
		{
			set_bit(mcu::register_map::spi::ctl0<N>, s_enable_mask);
		}

		static constexpr void disable() noexcept
		{
			reset_bit(mcu::register_map::spi::ctl0<N>, s_enable_mask);
		}

		static constexpr bool enabled() noexcept
		{
			return get_bit(mcu::register_map::spi::ctl0<N>, s_enable_mask);
		}

		template<state st> static constexpr void set_enabled() noexcept
		{
			set_enabled(st);
		}

		static constexpr void set_enabled(const state st) noexcept
		{
			st == state::on ? set_bit(mcu::register_map::spi::ctl0<N>, s_enable_mask) : unset_bit(mcu::register_map::spi::ctl0<N>, s_enable_mask);
		}

		static inline std::uint16_t read() noexcept
		{
			return *mcu::register_map::spi::data<N> & 0xffffu;
		}

		static inline void write(const std::uint16_t v) noexcept
		{
			*mcu::register_map::spi::data<N> = v;
		}

		static inline enum word_length word_length() noexcept
		{
			return get_bit(mcu::register_map::spi::ctl0<N>, s_word_length_mask) ? epl::spi::word_length::b16 : epl::spi::word_length::b8;
		}

		static inline void set_nss(const state st) noexcept
		{
			st == state::on ? set_bit(mcu::register_map::spi::ctl0<N>, s_sw_nss_mask) : reset_bit(mcu::register_map::spi::ctl0<N>, s_sw_nss_mask);
		}

		static constexpr void enable_irq_handling(basic_irq_handler_type* const h) noexcept
		{
			basic_irq_handler_type::set_instance(h);
			enable_irq();
		}

		static constexpr void disable_irq_handling() noexcept
		{
			disable_irq();
			basic_irq_handler_type::set_instance(nullptr);
		}

		static inline bool busy() noexcept
		{
			const auto status{*mcu::register_map::spi::stat<N>};

			return !(status & s_tbe_mask) || (status & s_trans_mask);
		}

	private:
		friend class dma::slot<port, dma::default_target>;

		static constexpr void enable_clock() noexcept
		{
			if constexpr (N == 0)
			{
				constexpr auto b{mcu::register_map::spi::if_bus<N>};
				rcu::enable_clock<b, clock_flag<N>>();
			}
		}

		static constexpr void enable_irq() noexcept
		{
			constexpr auto cause{irq::cause<N>};

			epl::irq::unit::configure(cause);
			epl::irq::unit::enable(cause);

			set_bit(mcu::register_map::spi::ctl1<N>, s_tbeie_mask | s_rbneie_mask | s_errie_mask);
		}

		static constexpr void disable_irq() noexcept
		{
			epl::irq::unit::disable(irq::cause<N>);
			reset_bit(mcu::register_map::spi::ctl1<N>, s_tbeie_mask | s_rbneie_mask | s_errie_mask);
		}

		static constexpr void set_mode(const mode m) noexcept
		{
			set_bit(mcu::register_map::spi::ctl0<N>, mode_bit(m) << 2);
		}
		
		static constexpr void set_duplex(const duplex d) noexcept
		{

		}

		static constexpr void set_word_length(const enum word_length l) noexcept
		{
			set_bit(mcu::register_map::spi::ctl0<N>, word_length_bit(l) << 11);
		}

		static constexpr void set_prescaler(const prescaler p) noexcept
		{
			set_bit(mcu::register_map::spi::ctl0<N>, prescaler_value(p));
		}

		static constexpr void set_bit_order(const bit_order o) noexcept
		{

		}

		static constexpr void set_nss_mode(const nss_mode nm) noexcept
		{
			switch (nm)
			{
			case nss_mode::input:
			case nss_mode::disabled:
				reset_bit(mcu::register_map::spi::ctl0<N>, static_cast<std::uint32_t>(1 << 9));
				reset_bit(mcu::register_map::spi::ctl1<N>, static_cast<std::uint32_t>(1 << 2));
				reset_bit(mcu::register_map::spi::ctl1<N>, static_cast<std::uint32_t>(1 << 3));
				reset_bit(mcu::register_map::spi::ctl1<N>, static_cast<std::uint32_t>(1 << 4));
				break;


			case nss_mode::pulse:
				set_bit(mcu::register_map::spi::ctl1<N>, static_cast<std::uint32_t>(1 << 3));
				reset_bit(mcu::register_map::spi::ctl1<N>, static_cast<std::uint32_t>(1 << 4));
				[[fallthrough]];

			default:
			case nss_mode::output:
				reset_bit(mcu::register_map::spi::ctl0<N>, static_cast<std::uint32_t>(1 << 9));
				set_bit(mcu::register_map::spi::ctl1<N>, static_cast<std::uint32_t>(1 << 2));
				break;

			case nss_mode::software:
				reset_bit(mcu::register_map::spi::ctl1<N>, static_cast<std::uint32_t>(1 << 2));
				reset_bit(mcu::register_map::spi::ctl1<N>, static_cast<std::uint32_t>(1 << 3));
				reset_bit(mcu::register_map::spi::ctl1<N>, static_cast<std::uint32_t>(1 << 4));
				set_bit(mcu::register_map::spi::ctl0<N>, static_cast<std::uint32_t>(1 << 8));
				set_bit(mcu::register_map::spi::ctl0<N>, static_cast<std::uint32_t>(1 << 9));
				break;

			case nss_mode::ti:
				set_bit(mcu::register_map::spi::ctl1<N>, static_cast<std::uint32_t>(1 << 2));
				reset_bit(mcu::register_map::spi::ctl1<N>, static_cast<std::uint32_t>(1 << 3));
				set_bit(mcu::register_map::spi::ctl1<N>, static_cast<std::uint32_t>(1 << 4));
				break;
			}
		}

		static constexpr std::uint32_t mode_bit(const mode m) noexcept
		{
			switch (m)
			{
			default:
			case mode::slave:
				return 0;

			case mode::master:
				return 1;
			}
		}

		static constexpr std::uint32_t word_length_bit(const enum word_length l) noexcept
		{
			switch (l)
			{
			default:
			case word_length::b8:
				return 0;

			case word_length::b16:
				return 1;
			}
		}

		static constexpr std::uint32_t prescaler_value(const prescaler p) noexcept
		{
			return static_cast<std::uint32_t>(p) << 3;
		}

		static constexpr std::uint32_t bit_order_bit(const bit_order o) noexcept
		{
			switch (o)
			{
			default:
			case bit_order::msb:
				return 0;

			case bit_order::lsb:
				return 1;
			}
		}

		template<dma::target t> static consteval volatile std::uint32_t* address() noexcept
		{
			switch (t)
			{
			default:
			case dma::target::data:
				return mcu::register_map::spi::data<N>;
			}
		}

		template<dma::target t, direction d> static constexpr void enable_dma() noexcept
		{
			constexpr std::uint32_t mask{1 << static_cast<std::uint32_t>(d == direction::out)};

			set_bit(mcu::register_map::spi::ctl1<N>, mask);
		}

		template<dma::target t, direction d> static constexpr void disable_dma() noexcept
		{
			constexpr std::uint32_t mask{1 << static_cast<std::uint32_t>(d == direction::out)};

			reset_bit(mcu::register_map::spi::ctl1<N>, mask);
		}

		static constexpr void reset() noexcept
		{
			// *mcu::register_map::spi::ctl0<N> = 0;
			// *mcu::register_map::spi::ctl1<N> = 0;
			// *mcu::register_map::spi::data<N> = 0;
			constexpr auto b{mcu::register_map::spi::if_bus<N>};
			set_bit(mcu::register_map::rcu::reset<b>, reset_flag<N>);
			reset_bit(mcu::register_map::rcu::reset<b>, reset_flag<N>);
		}

		static constexpr std::uint32_t s_enable_mask{1 << 6};
		static constexpr std::uint32_t s_mode_mask{1 << 2};
		static constexpr std::uint32_t s_nss_mode_mask{1 << 9};
		static constexpr std::uint32_t s_duplex_mask{1 << 10};
		static constexpr std::uint32_t s_word_length_mask{1 << 11};
		static constexpr std::uint32_t s_bit_order_mask{1 << 7};
		static constexpr std::uint32_t s_sw_nss_mask{1 << 8};

		static constexpr std::uint32_t s_tbeie_mask{1 << 7};
		static constexpr std::uint32_t s_rbneie_mask{1 << 6};
		static constexpr std::uint32_t s_errie_mask{1 << 5};

		static constexpr std::uint32_t s_tbe_mask{1 << 1};
		static constexpr std::uint32_t s_trans_mask{1 << 7};
	};
}

#endif // GD32VF103XX_EPL_SPI_H

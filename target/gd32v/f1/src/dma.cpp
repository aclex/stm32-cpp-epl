/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include "epl/dma.h"

using namespace std;

using namespace epl;
using namespace epl::dma;

namespace
{
	template<size_t Part, size_t Channel> void pass_irq() noexcept
	{
		auto* const h{dma::irq::basic_channel_handler<Part, Channel>::instance()};

		if (!h)
			return;

		h->resolve();
	}
}

namespace epl::dma::irq
{
	void h00() noexcept
	{
		pass_irq<0, 0>();
	}

	void h01() noexcept
	{
		pass_irq<0, 1>();
	}

	void h02() noexcept
	{
		pass_irq<0, 2>();
	}

	void h03() noexcept
	{
		pass_irq<0, 3>();
	}

	void h04() noexcept
	{
		pass_irq<0, 4>();
	}

	void h05() noexcept
	{
		pass_irq<0, 5>();
	}

	void h06() noexcept
	{
		pass_irq<0, 6>();
	}

	void h10() noexcept
	{
		pass_irq<1, 0>();
	}

	void h11() noexcept
	{
		pass_irq<1, 1>();
	}

	void h12() noexcept
	{
		pass_irq<1, 2>();
	}

	void h13() noexcept
	{
		pass_irq<1, 3>();
	}

	void h14() noexcept
	{
		pass_irq<1, 4>();
	}
}

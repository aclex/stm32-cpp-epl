/*
	EPL - peripheral library elements for microcontrollers
	Copyright (C) 2018-2021 Alexey Chernov

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

#include "epl/chrono.h"

#include <limits>

#include "epl/mcu/register_map.h"
#include "epl/rcu.h"
#include "epl/irq.h"
#include "epl/bit_ops.h"

using namespace std;

using namespace epl::chrono;

bool system_clock::s_started = false;
volatile epl::chrono::system_clock::rep epl::chrono::system_clock::s_ticks = 0;

namespace epl::chrono
{
	void timer_unit_tick() noexcept
	{
		*epl::mcu::register_map::timer_unit::mtime = 0;

		system_clock::s_ticks = system_clock::s_ticks + 1;
	}
}

extern "C" __attribute__((interrupt))
void eclic_mtip_handler() noexcept
{
	epl::irq::unit::context_holder cxt;

	timer_unit_tick();
}

void system_clock::start() noexcept
{
	if (s_started)
		return;
	else
		s_started = true;

	*mcu::register_map::timer_unit::mtime = 0;
	const auto count_to{rcu::to_ticks(std::chrono::duration_cast<std::chrono::microseconds>(duration{1}))};
	*mcu::register_map::timer_unit::mtimecmp = count_to;

	enable_irq();
}

void system_clock::enable_irq() noexcept
{
	epl::irq::unit::configure(s_irq_number, epl::irq::mode::vector, numeric_limits<uint8_t>::max(), numeric_limits<uint8_t>::max());
	epl::irq::unit::enable(s_irq_number);
}

MSTATUS_MIE = 0x8

CSR_WFE = 0x810
CSR_MSUBM = 0x7c4
CSR_MMISC_CTL = 0x7d0
CSR_MTVT = 0x307
CSR_MTVT2 = 0x7ec

# Assembly 'reset handler' function to initialize core CPU registers
.global reset_handler
.type reset_handler, @function
reset_handler:
	# Disable interrupts until they are needed
	csrc mstatus, MSTATUS_MIE

	# Move from 0x00000000 to 0x08000000 address space if necessary
	la   a0, reset_handler
	li   a1, 1 # constructing 0x08000000 constant at `a1` here
	slli a1, a1, 27 # and here
	bleu a1, a0, _start_0x08000000
	add  a0, a0, a1
	jr   a0

_start_0x08000000:
	# Load the initial stack pointer value
	la   sp, _sp

.option push
.option norelax
	la gp, __global_pointer$
.option pop
	la sp, _sp

	# load `data` section
	la a0, _sidata
	la a1, _sdata
	la a2, _edata
	bgeu a1, a2, _clear_bss
1:
	lw t0, 0(a0)
	sw t0, 0(a1)
	addi a0, a0, 4
	addi a1, a1, 4
	bltu a1, a2, 1b

_clear_bss:
	# clear `bss` section
	la a0, _sbss
	la a1, _ebss
	bgeu a0, a1, _init_irq
1:
	sw zero, 0(a0)
	addi a0, a0, 4
	bltu a0, a1, 1b

_init_irq:
	csrw mstatus, 0

	# enable WFI
	csrw CSR_WFE, 0

	# clear machine registers
	csrw mepc, 0
	csrw mcause, 0
	csrw CSR_MSUBM, 0

	# enable NMI_CAUSE_FFF
	li t0, 0x200
	csrw CSR_MMISC_CTL, t0

	la t0, vector_table
	csrw CSR_MTVT, t0

	#la t0, irq_entry
	la t0, non_vectorized_interrupt_handler
	csrw CSR_MTVT2, t0
	li t0, 0x03
	csrc CSR_MTVT2, t0
	csrs CSR_MTVT2, 0x1

	la t0, exception_handler_trap
	csrw mtvec, t0

	# enable ECLIC mode
	li t0, 0x3f
	csrc mtvec, t0
	csrs mtvec, 0x3

	call __libc_init_array

	# `main(0,0)`
	li   a0, 0
	li   a1, 0
	call main

	# init the remaining runtime
	la a0, __libc_fini_array
	call atexit

	j reset_handler

# a bunch of stubs - here not to be vanished during LTO
.global _kill
.type _kill, @function
_kill:
	ret

.global _getpid
.type _getpid, @function
_getpid:
	ret

.global _write
.type _write, @function
_write:
	ret

.global _close
.type _close, @function
_close:
	ret

.global _fstat
.type _fstat, @function
_fstat:
	ret

.global _isatty
.type _isatty, @function
_isatty:
	ret

.global _lseek
.type _lseek, @function
_lseek:
	ret

.global _read
.type _read, @function
_read:
	ret

.global reg_apb1_bus_base
reg_apb1_bus_base = 0x40000000
.global reg_apb2_bus_base
reg_apb2_bus_base = 0x40010000
.global reg_ahb1_bus_base
reg_ahb1_bus_base = 0x40018000
.global reg_ahb3_bus_base
reg_ahb3_bus_base = 0x60000000

# Timer unit

.global reg_timer_ctrl
reg_timer_ctrl = 0xd1000000 

# ECLIC

.global reg_eclic_base
reg_eclic_base = 0xd2000000

.global reg_eclic_info
reg_eclic_info = reg_eclic_base + 0x00000004
